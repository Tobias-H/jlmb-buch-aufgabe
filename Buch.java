/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;
    private int seiten;
    private String refNummer;
    private int ausgeliehen;
    private boolean kursText;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel, int buchseiten, boolean buchkurstext)
    {
        autor = buchautor;
        titel = buchtitel;
        seiten = buchseiten;
        refNummer = "";
        ausgeliehen = 0;
        kursText = buchkurstext;
    }

    
    /**
     * getter von autor
     */
    public String gibAutor(){
        return autor;
    }
    
    /**
     * Daten von autor auf Konsole ausgeben
     */
    public void autorAusgeben(){
        System.out.println(autor);
    }
    
    
    /**
     * getter von titel
     */
    public String gibTitel(){
        return titel;
    }
    
    /**
     * Daten von titel auf Konsole ausgeben
     */
    public void titelAusgeben(){
        System.out.println(titel);
    }
    
    
    /**
     * getter von seiten
     */
    public int gibSeiten(){
        return seiten;
    }
    
    
    /**
     * getter von refNummer
     */
    public String gibRefNummer(){
        return refNummer;
    }
    
    /**
     * setter von refNummer
     */
    public void setzteRefNummer(String ref){
        refNummer = ref;
    }
    

    /**
     * getter von ausgeliehen
     */
    public int gibAusgeliehen(){
        return ausgeliehen;
    }
    
    /**
     * Z�hlt, wie oft das Buch ausgeliehen wurde.
     */
    public void ausleihen(){
        ausgeliehen += 1;
    }
    
    
    /**
     * Gibt an, ob das Buch als Fachbuch in einem Kurs verwendet wird.
     */
    public boolean istKursText(){
        return kursText;
    }
    
    
    /**
     * Daten des Buches auf der Konsole ausgeben
     */
    public void detailsAusgeben(){
        String ref;
        if(refNummer.length() >= 3){
            ref = refNummer;
        } else {
            ref = "Ung�ltige Referenznummer!";
        }
        System.out.println("Autor: " + autor + "\nTitel: " + titel + 
                           "\nSeiten: " + seiten + "\nReferenznummer: " + ref +
                           "\nDas Buch wurde " + ausgeliehen + " mal ausgeliehen.");
    }
}
